package pl.vm.library.utils;

import static pl.vm.library.helper.DateConverter.convertLocalDateTimeToDate;

import java.time.LocalDateTime;

import pl.vm.library.entity.Book;
import pl.vm.library.entity.Reservation;
import pl.vm.library.entity.User;
import pl.vm.library.to.ReservationRenewalRequestTo;
import pl.vm.library.to.ReservationTo;

/**
 * Auxiliary class for holding unit test data
 */
public class TestData {
    public static final String RESERVATIONS_CREATE_URL = "/reservations/create";
    public static final String RESERVATIONS_RENEWAL_URL = "/reservations/renewal";

    public static final Long BOOK_ID = 11L;
    public static final Long USER_ID = 2L;
    public static final LocalDateTime DATE_FROM = LocalDateTime.of(2019, 9, 10, 1, 0, 0);
    public static final LocalDateTime DATE_TO = LocalDateTime.of(2019, 11, 10, 1, 0, 0);
    public static final Long RESERVATION_ID = 3L;
    public static final Long NON_EXISTING_RESERVATION_ID = 777L;

    public static ReservationTo createReservationTo(Long reservationId, Long bookId, Long userId, LocalDateTime dateFrom, LocalDateTime dateTo) {
        return ReservationTo.builder()
                .id(reservationId)
                .bookId(bookId)
                .userId(userId)
                .fromDate(dateFrom)
                .toDate(dateTo)
                .build();
    }

    public static Reservation createReservation(Long reservationId, Long bookId, Long userId, LocalDateTime dateFrom, LocalDateTime dateTo){
        Reservation reservation = new Reservation();
        reservation.setId(reservationId);

        Book book = new Book();
        book.setId(bookId);
        reservation.setBook(book);

        User user = new User();
        user.setId(userId);
        reservation.setUser(user);

        reservation.setFromDate(convertLocalDateTimeToDate(dateFrom));
        reservation.setToDate(convertLocalDateTimeToDate(dateTo));

        return reservation;
    }

    public static ReservationRenewalRequestTo createReservationRenewalRequestTo(LocalDateTime dateTo, Long reservationId) {
        return ReservationRenewalRequestTo.builder()
                .dateTo(dateTo)
                .id(reservationId)
                .build();
    }
}
