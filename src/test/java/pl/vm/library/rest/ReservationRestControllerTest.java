package pl.vm.library.rest;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static pl.vm.library.utils.TestData.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import pl.vm.library.exception.ParameterValidationException;
import pl.vm.library.service.impl.ReservationServiceImpl;
import pl.vm.library.to.ReservationRenewalRequestTo;
import pl.vm.library.to.ReservationTo;
import pl.vm.library.utils.TestData;

/**
 * Test class for {@link ReservationRestController}
 */
@RunWith(SpringRunner.class)
@WebMvcTest(ReservationRestController.class)
public class ReservationRestControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private ReservationServiceImpl reservationService;

    private ObjectMapper mapper = new ObjectMapper();

    @Before
    public void init() {
        mapper.registerModule(new JavaTimeModule());
        mapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
    }

    @Test
    public void shouldCreateReservation() throws Exception {
        //given
        ReservationTo request = createReservationTo(null, BOOK_ID, USER_ID, DATE_FROM, DATE_TO);
        when(reservationService.createReservation(any())).thenReturn(new ReservationTo());

        //when & then
        mvc.perform(post(TestData.RESERVATIONS_CREATE_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isCreated());
        verify(reservationService, times(1)).createReservation(any());
    }

    @Test
    public void shouldThrowExceptionForCreateReservationRequestWithReservationId() throws Exception {
        //given
        ReservationTo request = createReservationTo(RESERVATION_ID, BOOK_ID, USER_ID, DATE_FROM, DATE_TO);
        when(reservationService.createReservation(any())).thenThrow(new ParameterValidationException("When creating new Reservation, the ID should be null."));

        //when & then
        mvc.perform(post(TestData.RESERVATIONS_CREATE_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isBadRequest());
        verify(reservationService, times(1)).createReservation(any());
    }

    @Test
    public void updateReservationToDate() throws Exception {
        //given
        ReservationRenewalRequestTo request = createReservationRenewalRequestTo(DATE_TO, RESERVATION_ID);

        when(reservationService.updateReservationToDate(any())).thenReturn(new ReservationTo());
        //when & then
        mvc.perform(put(TestData.RESERVATIONS_RENEWAL_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isOk());
        verify(reservationService, times(1)).updateReservationToDate(any());
    }

    @Test
    public void shouldThrowExceptionForRenewalRequestWithoutReservationId() throws Exception {
        //given
        ReservationRenewalRequestTo request = createReservationRenewalRequestTo(DATE_TO, null);
        //when & then
        mvc.perform(put(TestData.RESERVATIONS_RENEWAL_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(mapper.writeValueAsString(request)))
                .andExpect(status().isBadRequest());
    }
}