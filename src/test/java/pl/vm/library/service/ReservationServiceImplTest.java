package pl.vm.library.service;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static pl.vm.library.helper.DateConverter.convertLocalDateTimeToDate;
import static pl.vm.library.utils.TestData.*;

import java.util.Optional;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import pl.vm.library.entity.Reservation;
import pl.vm.library.exception.EntityWithProvidedIdNotFoundException;
import pl.vm.library.exception.ParameterValidationException;
import pl.vm.library.repository.ReservationRepository;
import pl.vm.library.service.impl.ReservationServiceImpl;
import pl.vm.library.to.ReservationRenewalRequestTo;
import pl.vm.library.to.ReservationTo;
import pl.vm.library.utils.TestData;

/**
 * Test class for {@link ReservationService}
 */
@RunWith(MockitoJUnitRunner.class)
public class ReservationServiceImplTest {

    @Before
    public void init() {
        reservationService.initMapper();
    }

    @Mock
    private ReservationRepository reservationRepository;

    @InjectMocks
    private ReservationServiceImpl reservationService;

    private ArgumentCaptor<Reservation> reservationArgumentCaptor = ArgumentCaptor.forClass(Reservation.class);

    @Test
    public void shouldCreateNewReservation() {
        //given
        ReservationTo request = createReservationTo(null, BOOK_ID, USER_ID, DATE_FROM, DATE_TO);
        Reservation reservation = createReservation(null, BOOK_ID, USER_ID, DATE_FROM, DATE_TO);

        when(reservationRepository.save(any())).thenReturn(reservation);
        //when
        reservationService.createReservation(request);
        //then
        verify(reservationRepository, times(1)).save(reservationArgumentCaptor.capture());
        assertEquals(BOOK_ID, reservationArgumentCaptor.getValue().getBook().getId());
        assertEquals(TestData.USER_ID, reservationArgumentCaptor.getValue().getUser().getId());
    }

    @Test
    public void shouldUpdateReservationToDate() {
        //given
        ReservationRenewalRequestTo request = createReservationRenewalRequestTo(DATE_TO, RESERVATION_ID);
        ReservationTo reservationTo = createReservationTo(null, BOOK_ID, USER_ID, DATE_FROM, DATE_FROM);
        Reservation reservationWithBothFromDates = createReservation(null, BOOK_ID, USER_ID, DATE_FROM, DATE_FROM);

        when(reservationRepository.findById(any())).thenReturn(Optional.of(reservationWithBothFromDates));
        //when
        reservationService.updateReservationToDate(request);
        //then
        verify(reservationRepository, times(1)).save(reservationArgumentCaptor.capture());
        assertEquals(convertLocalDateTimeToDate(DATE_TO), reservationArgumentCaptor.getValue().getToDate());
    }

    @Test
    public void shouldThrowExceptionOnUpdateReservationToDateForNonExistingReservation() {
        //given
        ReservationRenewalRequestTo request = createReservationRenewalRequestTo(DATE_TO, NON_EXISTING_RESERVATION_ID);

        when(reservationRepository.findById(NON_EXISTING_RESERVATION_ID)).thenReturn(Optional.empty());
        try {
            //when
            reservationService.updateReservationToDate(request);
            fail();
        } catch (Exception e) {
            //then
            assertEquals(EntityWithProvidedIdNotFoundException.class, e.getClass());
        }
    }

    @Test
    public void shouldThrowExceptionForNewToDateEarlierThanSavedToDate() {
        //given
        ReservationRenewalRequestTo request = createReservationRenewalRequestTo(DATE_FROM, RESERVATION_ID);
        Reservation reservationWithBothToDates = createReservation(null, BOOK_ID, USER_ID, DATE_FROM, DATE_TO);

        when(reservationRepository.findById(any())).thenReturn(Optional.of(reservationWithBothToDates));
        try {
            //when
            reservationService.updateReservationToDate(request);
            fail();
        } catch (Exception e) {
            //then
            assertEquals(ParameterValidationException.class, e.getClass());
        }
    }

}