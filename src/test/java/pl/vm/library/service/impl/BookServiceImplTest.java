package pl.vm.library.service.impl;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;
import static pl.vm.library.utils.TestData.BOOK_ID;

import java.util.Optional;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import pl.vm.library.entity.Book;
import pl.vm.library.entity.Reservation;
import pl.vm.library.exception.EntityInUsageException;
import pl.vm.library.exception.EntityWithProvidedIdNotFoundException;
import pl.vm.library.repository.BookRepository;

/**
 * Test class for {@link BookServiceImpl}
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BookServiceImpl.class)
public class BookServiceImplTest {

    @MockBean
    private BookRepository bookRepository;

    @Autowired
    private BookServiceImpl bookService;

    @Test
    public void shouldDeleteBook() {
        //given
        Book book = new Book();
        book.setReservations(Lists.newArrayList());
        when(bookRepository.findById(BOOK_ID)).thenReturn(Optional.of(book));
        //when
        bookService.delete(BOOK_ID);
        //then
        verify(bookRepository, times(1)).findById(BOOK_ID);
        verify(bookRepository, times(1)).delete(book);
    }

    @Test
    public void shouldThrowExceptionIfBookForDeletionHasReservations() {
        //given
        Book book = new Book();
        book.setReservations(Lists.newArrayList(new Reservation()));
        when(bookRepository.findById(BOOK_ID)).thenReturn(Optional.of(book));
        try {
            //when
            bookService.delete(BOOK_ID);
            fail();
        } catch (Exception e) {
            //then
            assertEquals(EntityInUsageException.class, e.getClass());
        }
    }

    @Test
    public void shouldThrowExceptionIfBookForDeletionDoesNotExist() {
        //given
        when(bookRepository.findById(BOOK_ID)).thenReturn(Optional.empty());
        try {
            //when
            bookService.delete(BOOK_ID);
            fail();
        } catch (Exception e) {
            //then
            assertEquals(EntityWithProvidedIdNotFoundException.class, e.getClass());
        }
    }
}
