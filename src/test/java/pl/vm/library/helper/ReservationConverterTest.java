package pl.vm.library.helper;

import static org.junit.Assert.assertEquals;
import static pl.vm.library.helper.DateConverter.convertLocalDateTimeToDate;
import static pl.vm.library.helper.ReservationConverter.RESERVATION_CONVERTER;
import static pl.vm.library.helper.ReservationConverter.RESERVATION_TO_CONVERTER;
import static pl.vm.library.utils.TestData.*;

import org.junit.Before;
import org.junit.Test;
import org.modelmapper.ModelMapper;

import pl.vm.library.entity.Reservation;
import pl.vm.library.to.ReservationTo;

/**
 * Test class for {@link ReservationConverter}
 */
public class ReservationConverterTest {

    private ModelMapper modelMapper;

    @Before
    public void init() {
        modelMapper = new ModelMapper();
        modelMapper.addConverter(RESERVATION_CONVERTER);
        modelMapper.addConverter(RESERVATION_TO_CONVERTER);
    }

    @Test
    public void shouldConvertTOToEntity() {
        //given
        ReservationTo reservationTo = createReservationTo(RESERVATION_ID, BOOK_ID, USER_ID, DATE_FROM, DATE_TO);
        //when
        Reservation result = modelMapper.map(reservationTo, Reservation.class);
        //then
        assertEquals(RESERVATION_ID, result.getId());
        assertEquals(BOOK_ID, result.getBook().getId());
        assertEquals(USER_ID, result.getUser().getId());
        assertEquals(convertLocalDateTimeToDate(DATE_FROM), result.getFromDate());
        assertEquals(convertLocalDateTimeToDate(DATE_TO), result.getToDate());
    }

    @Test
    public void shouldConvertEntityToTO() {
        //given
        Reservation reservation = createReservation(RESERVATION_ID, BOOK_ID, USER_ID, DATE_FROM, DATE_TO);
        //when
        ReservationTo result = modelMapper.map(reservation, ReservationTo.class);
        //then
        assertEquals(RESERVATION_ID, result.getId());
        assertEquals(BOOK_ID, result.getBookId());
        assertEquals(USER_ID, result.getUserId());
        assertEquals(DATE_FROM, result.getFromDate());
        assertEquals(DATE_TO, result.getToDate());
    }

}