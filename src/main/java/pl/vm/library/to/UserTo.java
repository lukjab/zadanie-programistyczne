package pl.vm.library.to;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

/**
 * Transport Object for the User class.
 * 
 * Contains also the Reservation list of the given User.
 */
@Getter
@Setter
public class UserTo implements Serializable {

	private static final long serialVersionUID = 5327766680748073213L;

	private Long id;

	@NotNull
	@Size(min = 6, max = 255)
	private String login;

	@NotNull
	@Size(max = 255)
	private String name;

	@NotNull
	@Size(max = 255)
	private String lastName;

	@NotNull
	@Email
	@Size(max = 255)
	private String email;

	private List<ReservationTo> reservations = new ArrayList<>();

}
