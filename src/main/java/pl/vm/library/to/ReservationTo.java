package pl.vm.library.to;

import static pl.vm.library.constant.CommonConstants.DEFAULT_DATE_TIME_FORMAT;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.*;

/**
 * Transport Object of the Reservation class.
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ReservationTo implements Serializable {

    private static final long serialVersionUID = -60690548233543094L;

    private Long id;

    @NotNull
    private Long userId;

    @NotNull
    private Long bookId;

    @NotNull
    @JsonFormat(pattern = DEFAULT_DATE_TIME_FORMAT)
    private LocalDateTime fromDate;

    @NotNull
    @JsonFormat(pattern = DEFAULT_DATE_TIME_FORMAT)
    private LocalDateTime toDate;

}