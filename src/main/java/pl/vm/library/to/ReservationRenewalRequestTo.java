package pl.vm.library.to;

import static pl.vm.library.constant.CommonConstants.DEFAULT_DATE_TIME_FORMAT;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;
import com.fasterxml.jackson.annotation.JsonFormat;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Transport object for reservation renewal request
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ReservationRenewalRequestTo implements Serializable {

    private static final long serialVersionUID = 2923737828439703647L;

    @NotNull
    private Long id;

    @NotNull
    @JsonFormat(pattern = DEFAULT_DATE_TIME_FORMAT)
    private LocalDateTime dateTo;

}
