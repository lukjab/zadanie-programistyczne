package pl.vm.library.validator;

import org.apache.commons.collections4.CollectionUtils;

import pl.vm.library.exception.ParameterValidationException;
import pl.vm.library.to.UserTo;

/**
 * Auxiliary class used for validating user requests
 */
public class UserValidator {

    /**
     * Validate new user creation request
     *
     * @param userTo {@link UserTo}
     */
    public static void validateNewUser(UserTo userTo) {
        if (userTo.getId() != null) {
            throw new ParameterValidationException("When creating new User, the ID should be null.");
        }

        if (CollectionUtils.isNotEmpty(userTo.getReservations())) {
            throw new ParameterValidationException(
                    "When creating new User the Reservation list should be initially empty.");
        }
    }
}
