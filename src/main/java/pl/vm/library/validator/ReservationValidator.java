package pl.vm.library.validator;

import java.time.LocalDateTime;

import pl.vm.library.exception.ParameterValidationException;
import pl.vm.library.to.ReservationTo;

/**
 * Auxiliary class used for validating reservation requests
 */
public class ReservationValidator {

    /**
     * Validates if new toDate is after previously saved reservation's toDate
     *
     * @param previouslySavedToDate toDate retrieved from DB
     * @param newToDate new toDate from request
     */
    public static void validateReservationRenewalDate(LocalDateTime previouslySavedToDate, LocalDateTime newToDate) {
        if(!previouslySavedToDate.isBefore(newToDate)) {
            throw new ParameterValidationException("Provided 'to' date is earlier than currently saved 'to' date for reservation");
        }
    }

    /**
     * Validates if reservation creation request does not have reservation id
     *
     * @param request {@link ReservationTo}
     */
    public static void validateNewReservationRequest(ReservationTo request) {
        if (request.getId() != null) {
            throw new ParameterValidationException("When creating new Reservation, the ID should be null.");
        }
    }
}
