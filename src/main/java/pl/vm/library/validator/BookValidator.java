package pl.vm.library.validator;

import pl.vm.library.exception.ParameterValidationException;
import pl.vm.library.to.BookTo;

/**
 * Auxiliary class used for validating book requests
 */
public class BookValidator {

    /**
     * Validates new book creation request
     *
     * @param book {@link BookTo}
     */
    public static void validateNewBook(BookTo book) {
        if (book.getId() != null) {
            throw new ParameterValidationException("When creating new Book, the ID should be null.");
        }
    }
}
