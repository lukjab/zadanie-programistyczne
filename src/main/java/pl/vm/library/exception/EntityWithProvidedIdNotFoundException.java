package pl.vm.library.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception used when trying to operate on Entities which don't exist in the
 * Database.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class EntityWithProvidedIdNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 518405525882996153L;

	public EntityWithProvidedIdNotFoundException() {
		super("The Entity with the given ID was not found.");
	}

	public EntityWithProvidedIdNotFoundException(String message) {
		super(message);
	}
}
