package pl.vm.library.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception used when trying to modify entities that are currently in use and cannot be modified
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class EntityInUsageException extends RuntimeException {

    private static final long serialVersionUID = 3900421745800684911L;

    public EntityInUsageException() {
        super("The Entity with the given ID is used in system and delete operation cannot be performed.");
    }

    public EntityInUsageException(String message) {
        super(message);
    }
}
