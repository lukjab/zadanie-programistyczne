package pl.vm.library.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception used when operations are tried to made with wrong input parameters.
 */
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class ParameterValidationException extends RuntimeException {

	private static final long serialVersionUID = -779041494072937362L;

	public ParameterValidationException() {
		super("One of the input parameters is not correct");
	}

	public ParameterValidationException(String message) {
		super(message);
	}
}
