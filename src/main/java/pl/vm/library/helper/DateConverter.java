package pl.vm.library.helper;

import java.util.Date;
import java.time.LocalDateTime;
import java.time.ZoneId;

/**
 * Auxiliary class used for conversion between Date and LocalDateTime
 */
public class DateConverter {

    public static Date convertLocalDateTimeToDate(LocalDateTime dateFrom) {
        return Date.from(dateFrom.atZone(ZoneId.systemDefault()).toInstant());
    }

    public static LocalDateTime convertDateToLocalDateTime(java.util.Date toDate) {
        return toDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }
}
