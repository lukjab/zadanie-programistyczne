package pl.vm.library.helper;

import static pl.vm.library.helper.DateConverter.convertDateToLocalDateTime;
import static pl.vm.library.helper.DateConverter.convertLocalDateTimeToDate;

import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;

import pl.vm.library.entity.Book;
import pl.vm.library.entity.Reservation;
import pl.vm.library.entity.User;
import pl.vm.library.to.ReservationTo;

/**
 * Class responsible for holding reservation converters between entity and transfer object
 */
public class ReservationConverter {

    public static final Converter<ReservationTo, Reservation> RESERVATION_CONVERTER = new Converter<ReservationTo, Reservation>() {
        @Override
        public Reservation convert(MappingContext<ReservationTo, Reservation> mappingContext) {
            Reservation destination = new Reservation();
            ReservationTo source = mappingContext.getSource();
            destination.setId(source.getId());

            Book book = new Book();
            book.setId(source.getBookId());
            destination.setBook(book);

            User user = new User();
            user.setId(source.getUserId());
            destination.setUser(user);

            destination.setFromDate(convertLocalDateTimeToDate(source.getFromDate()));
            destination.setToDate(convertLocalDateTimeToDate(source.getToDate()));

            return destination;
        }
    };

    public static final Converter<Reservation, ReservationTo> RESERVATION_TO_CONVERTER = new Converter<Reservation, ReservationTo>() {
        @Override
        public ReservationTo convert(MappingContext<Reservation, ReservationTo> mappingContext) {
            Reservation source = mappingContext.getSource();

            ReservationTo destination = new ReservationTo();
            destination.setId(source.getId());
            destination.setToDate(convertDateToLocalDateTime(source.getToDate()));
            destination.setFromDate(convertDateToLocalDateTime(source.getFromDate()));
            destination.setUserId(source.getUser().getId());
            destination.setBookId(source.getBook().getId());

            return destination;
        }
    };

}
