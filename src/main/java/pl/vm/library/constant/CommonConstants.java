package pl.vm.library.constant;

/**
 * Auxiliary class used for holding application constant values
 */
public class CommonConstants {

    public static final String DEFAULT_DATE_TIME_FORMAT = "yyyy/MM/dd HH:mm:ss";
}
