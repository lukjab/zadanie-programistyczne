package pl.vm.library.service.impl;

import static pl.vm.library.helper.DateConverter.convertDateToLocalDateTime;
import static pl.vm.library.helper.DateConverter.convertLocalDateTimeToDate;
import static pl.vm.library.helper.ReservationConverter.RESERVATION_CONVERTER;
import static pl.vm.library.helper.ReservationConverter.RESERVATION_TO_CONVERTER;
import static pl.vm.library.validator.ReservationValidator.validateNewReservationRequest;
import static pl.vm.library.validator.ReservationValidator.validateReservationRenewalDate;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.function.Function;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import pl.vm.library.entity.Reservation;
import pl.vm.library.exception.EntityWithProvidedIdNotFoundException;
import pl.vm.library.repository.ReservationRepository;
import pl.vm.library.service.ReservationService;
import pl.vm.library.to.ReservationRenewalRequestTo;
import pl.vm.library.to.ReservationTo;

@Service
@Transactional
public class ReservationServiceImpl implements ReservationService {

    private final ReservationRepository reservationRepository;

    private ModelMapper mapper = new ModelMapper();

    @Autowired
    public ReservationServiceImpl(ReservationRepository reservationRepository) {
        this.reservationRepository = reservationRepository;
    }

    @PostConstruct
    public void initMapper() {
        mapper.addConverter(RESERVATION_CONVERTER);
        mapper.addConverter(RESERVATION_TO_CONVERTER);
    }

    @Override
    public ReservationTo createReservation(ReservationTo reservationCreationRequest) {
        validateNewReservationRequest(reservationCreationRequest);

        Reservation reservationToCreate = mapper.map(reservationCreationRequest, Reservation.class);

        try {
            Reservation createdReservation = reservationRepository.save(reservationToCreate);
            return mapper.map(createdReservation, ReservationTo.class);
        } catch (DataIntegrityViolationException e) {
            throw new EntityWithProvidedIdNotFoundException("Either book or user with provided id was not found");
        }
    }

    @Override
    public ReservationTo updateReservationToDate(ReservationRenewalRequestTo reservationRenewalRequest) {
        Optional<Reservation> reservationToUpdate = reservationRepository.findById(reservationRenewalRequest.getId());

        return reservationToUpdate.map(updateToDate(reservationRenewalRequest.getDateTo()))
                .orElseThrow(() -> new EntityWithProvidedIdNotFoundException("Reservation with provided id was not found"));
    }

    private Function<Reservation, ReservationTo> updateToDate(LocalDateTime newToDate) {
        return reservation -> {
            LocalDateTime oldToDate = convertDateToLocalDateTime(reservation.getToDate());
            validateReservationRenewalDate(oldToDate, newToDate);

            reservation.setToDate(convertLocalDateTimeToDate(newToDate));
            reservationRepository.save(reservation);

            return mapper.map(reservation, ReservationTo.class);
        };
    }

}
