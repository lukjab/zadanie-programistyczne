package pl.vm.library.service.impl;

import static pl.vm.library.validator.BookValidator.validateNewBook;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.commons.collections4.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.vm.library.entity.Book;
import pl.vm.library.exception.EntityInUsageException;
import pl.vm.library.exception.EntityWithProvidedIdNotFoundException;
import pl.vm.library.repository.BookRepository;
import pl.vm.library.service.BookService;
import pl.vm.library.to.BookTo;

@Service
@Transactional
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;

    private ModelMapper mapper = new ModelMapper();

    @Autowired
    public BookServiceImpl(BookRepository bookRepository) {
        this.bookRepository = bookRepository;
    }

    @Override
    public List<BookTo> findAll() {
        List<Book> books = (List<Book>) bookRepository.findAll();

        return books.stream()
                .map(bookEntity -> mapper.map(bookEntity, BookTo.class))
                .collect(Collectors.toList());
    }

    @Override
    public BookTo findById(Long id) {
        return bookRepository.findById(id)
                .map(bookEntity -> mapper.map(bookEntity, BookTo.class))
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public BookTo create(BookTo bookTo) {
        validateNewBook(bookTo);

        Book bookEntity = mapper.map(bookTo, Book.class);

        bookRepository.save(bookEntity);

        return mapper.map(bookEntity, BookTo.class);
    }

    @Override
    public void delete(Long id) {
        Optional<Book> book = bookRepository.findById(id);

        book.map(this::deleteBookWithoutReservations)
                .orElseThrow(() -> new EntityWithProvidedIdNotFoundException("Book that should be deleted could not be found"));
    }

    private Book deleteBookWithoutReservations(Book book) {
        if (CollectionUtils.isNotEmpty(book.getReservations())) {
            throw new EntityInUsageException("Book can not be deleted because it has active reservations");
        } else {
            bookRepository.delete(book);
        }

        return book;
    }

}
