package pl.vm.library.service.impl;

import static pl.vm.library.validator.UserValidator.validateNewUser;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pl.vm.library.entity.User;
import pl.vm.library.exception.EntityWithProvidedIdNotFoundException;
import pl.vm.library.repository.UserRepository;
import pl.vm.library.service.UserService;
import pl.vm.library.to.UserTo;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	private final UserRepository userRepository;

	private ModelMapper mapper = new ModelMapper();

	@Autowired
	public UserServiceImpl(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public List<UserTo> findAll() {
		List<User> allUsers = (List<User>) userRepository.findAll();

		return allUsers.stream()
				.map(userEntity -> mapper.map(userEntity, UserTo.class))
				.collect(Collectors.toList());
	}

	@Override
	public UserTo findById(Long id) {
		return userRepository.findById(id)
				.map(userEntity -> mapper.map(userEntity, UserTo.class))
				.orElseThrow(EntityWithProvidedIdNotFoundException::new);
	}

	@Override
	public UserTo create(UserTo userTo) {
		validateNewUser(userTo);

		User userEntity = mapper.map(userTo, User.class);
		userEntity = userRepository.save(userEntity);

		return mapper.map(userEntity, UserTo.class);
	}

}
