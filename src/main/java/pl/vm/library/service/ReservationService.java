package pl.vm.library.service;

import pl.vm.library.to.ReservationRenewalRequestTo;
import pl.vm.library.to.ReservationTo;

/**
 * The Service which contains business logic for Reservation.
 */
public interface ReservationService {

    /**
     * Creates new reservation
     *
     * @param reservationCreationRequest {@link ReservationTo}
     * @return the persisted reservation
     */
    ReservationTo createReservation(ReservationTo reservationCreationRequest);

    /**
     * Updated reservation toDate to new value
     *
     * @param reservationRenewalRequest {@link ReservationRenewalRequestTo}
     * @return the updated reservation
     */
    ReservationTo updateReservationToDate(ReservationRenewalRequestTo reservationRenewalRequest);
}
