package pl.vm.library.service;

import java.util.List;

import pl.vm.library.to.BookTo;

/**
 * The Service which contains business logic for Book.
 */
public interface BookService {

    /**
     * Returns all Books.
     *
     * @return all Books
     */
    List<BookTo> findAll();

    /**
     * Returns the Book with the given ID.
     *
     * @return the found Book
     */
    BookTo findById(Long id);

    /**
     * Creates a new Entity for the given object.
     *
     * @param book {@link BookTo}
     * @return the persisted Book
     */
    BookTo create(BookTo book);

    /**
     * Deletes the Book with the given id.
     *
     * @param id book's id
     */
    void delete(Long id);

}
