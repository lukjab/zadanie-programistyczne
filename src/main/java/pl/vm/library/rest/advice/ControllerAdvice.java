package pl.vm.library.rest.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import pl.vm.library.exception.EntityInUsageException;
import pl.vm.library.exception.EntityWithProvidedIdNotFoundException;
import pl.vm.library.exception.ParameterValidationException;

/**
 * Auxiliary class used for defining global exception handling strategies
 */
@RestControllerAdvice
public class ControllerAdvice {

    @ExceptionHandler(EntityWithProvidedIdNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ResponseMessage handleNotFoundException(EntityWithProvidedIdNotFoundException ex) {
        return new ResponseMessage(ex.getMessage());
    }

    @ExceptionHandler(EntityInUsageException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseMessage handleEntityInUsageException(EntityInUsageException ex){
        return new ResponseMessage(ex.getMessage());
    }

    @ExceptionHandler(ParameterValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseMessage handleParameterValidationException(ParameterValidationException ex) {
        return new ResponseMessage(ex.getMessage());
    }

}
