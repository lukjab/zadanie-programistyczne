package pl.vm.library.rest.advice;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Auxiliary POJO class used for limiting information returned in response in case of exception
 */
@AllArgsConstructor
@Getter
public class ResponseMessage {
    private String message;
}
