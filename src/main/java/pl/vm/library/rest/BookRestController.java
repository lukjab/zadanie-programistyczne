package pl.vm.library.rest;

import javax.validation.Valid;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import lombok.extern.slf4j.Slf4j;
import pl.vm.library.service.BookService;
import pl.vm.library.to.BookTo;

@RestController
@RequestMapping("/books")
@Slf4j
public class BookRestController {

	private final BookService bookService;

	@Autowired
	public BookRestController(BookService bookService) {
		this.bookService = bookService;
	}

	@ResponseStatus(HttpStatus.OK)
	@GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
	public List<BookTo> findAll() {
		log.info("Received findAll books request");

		return bookService.findAll();
	}

	@ResponseStatus(HttpStatus.OK)
	@GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public BookTo findById(@PathVariable Long id) {
		log.info("Received findById request for book id: {}", id);

		return bookService.findById(id);
	}

	@ResponseStatus(HttpStatus.CREATED)
	@PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public BookTo create(@Valid @RequestBody BookTo book) {
		log.info("Received create book request for author: {}, title: {}, isbn:{}", book.getAuthor(), book.getTitle(), book.getIsbn());

		return bookService.create(book);
	}

	@ResponseStatus(HttpStatus.OK)
	@DeleteMapping(value = "/{id}")
	public void delete(@PathVariable Long id) {
		log.info("Received delete request for book id: {}", id);

		bookService.delete(id);
	}

}
