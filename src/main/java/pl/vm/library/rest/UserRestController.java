package pl.vm.library.rest;

import javax.validation.Valid;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import lombok.extern.slf4j.Slf4j;
import pl.vm.library.service.UserService;
import pl.vm.library.to.UserTo;

@RestController
@RequestMapping("/users")
@Slf4j
public class UserRestController {

    private final UserService userService;

    @Autowired
    public UserRestController(UserService userService) {
        this.userService = userService;
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<UserTo> findAll() {
        log.info("Received findAll users request");

        return userService.findAll();
    }

    @ResponseStatus(HttpStatus.OK)
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserTo findById(@PathVariable Long id) {
        log.info("Received findById request for user id: {}", id);

        return userService.findById(id);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public UserTo create(@Valid @RequestBody UserTo user) {
        log.info("Received create user request for login: {}, name: {}, lastName: {}, email: {}", user.getLogin(), user.getName(), user.getLastName(), user.getEmail());

        return userService.create(user);
    }
}
