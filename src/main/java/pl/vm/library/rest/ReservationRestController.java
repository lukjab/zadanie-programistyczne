package pl.vm.library.rest;

import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import lombok.extern.slf4j.Slf4j;
import pl.vm.library.service.ReservationService;
import pl.vm.library.to.ReservationRenewalRequestTo;
import pl.vm.library.to.ReservationTo;

@RestController
@RequestMapping("/reservations")
@Slf4j
public class ReservationRestController {

    private final ReservationService reservationService;

    @Autowired
    public ReservationRestController(ReservationService reservationService) {
        this.reservationService = reservationService;
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(value = "/create", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ReservationTo createReservation(@RequestBody @Valid ReservationTo request) {
        log.info("Received createReservation request for reservation for book id: {}, user id: {}", request.getBookId(), request.getUserId());

        return reservationService.createReservation(request);
    }

    @ResponseStatus(HttpStatus.OK)
    @PutMapping(value = "/renewal", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ReservationTo updateReservationToDate(@RequestBody @Valid ReservationRenewalRequestTo request) {
        log.info("Received updateReservationToDate request for reservation id: {}", request.getId());

        return reservationService.updateReservationToDate(request);
    }

}
