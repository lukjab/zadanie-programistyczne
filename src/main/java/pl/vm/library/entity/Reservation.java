package pl.vm.library.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "reservation")
@Getter
@Setter
public class Reservation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private User user;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "book_id")
	private Book book;

	@Column
	@NotNull
	private Date fromDate;

	@Column
	@NotNull
	private Date toDate;
}
