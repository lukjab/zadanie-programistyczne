package pl.vm.library.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Entity
@Table(name = "book")
@Getter
@Setter
public class Book {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column
	@NotNull
	@Size(max = 255)
	private String author;

	@Column
	@NotNull
	@Size(max = 255)
	private String title;

	@Column
	@NotNull
	@Size(min = 13, max = 13)
	private String isbn;

	@Column
	private Date releaseDate;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "book")
	private List<Reservation> reservations;

}
